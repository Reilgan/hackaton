FROM python:3.10

WORKDIR  /srv/hack/

COPY ./setup.py ./alembic.ini ./

RUN pip install -e ./[testing]

COPY ./app ./app
COPY ./alembic ./alembic
COPY ./start.sh ./start.sh
RUN chmod -R 777 ./start.sh

CMD ["./start.sh"]