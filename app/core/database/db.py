from contextlib import contextmanager, AbstractContextManager
from typing import Callable
import logging

from sqlalchemy import Engine, create_engine
from sqlalchemy.orm import Session, declarative_base, scoped_session, sessionmaker

from app.settings import DATABASE_URL

logger = logging.getLogger(__name__)


OrmBaseModel = declarative_base()


class DataBase:
    def __init__(self, engine: Engine | None = None) -> None:
        self._engine = engine
        if self._engine:
            self.__configure()

    def __configure(self):
        self._session_factory = scoped_session(
            sessionmaker(
                autocommit=False,
                autoflush=False,
                bind=self._engine,
            ),
        )

    def configure(self, engine: Engine):
        self._engine = engine
        self.__configure()

    def create_database(self) -> None:
        OrmBaseModel.metadata.create_all(self._engine)

    @contextmanager
    def session(self) -> Callable[..., AbstractContextManager[Session]]:
        session: Session = self._session_factory()
        try:
            yield session
        except Exception:
            logger.exception("Session rollback because of exception")
            session.rollback()
            raise
        finally:
            session.commit()
            session.close()


engine = create_engine(DATABASE_URL, echo=False)
data_base = DataBase(engine)
