from typing import Union, Optional, Any
import logging
from datetime import timedelta
from app.services.cache.base_cache import Cache
from redis.asyncio import Redis

logger = logging.getLogger(__name__)


class RedisCache(Cache):

    url: str = None

    @classmethod
    def configure(cls, url: str):
        cls.url = url

    def __init__(self):
        self._redis: Optional[Redis] = None

    async def connect(self):
        logger.info(f'Redis cache connected to {self.url}')
        self._redis = Redis.from_url(self.url)

    async def disconnect(self):
        await self._redis.close()

    async def get(self, key: str) -> Any:
        logger.debug(f'Returning value by key {key}')
        try:
            return await self._redis.get(key)
        except Exception as err:
            logger.warning(f'Redis Cache, get by key error: {str(err)}')
            return None

    async def set(self, key: str, value: Any, ttl: Union[int, timedelta, None] = None):
        await self._redis.set(key, value)
        logger.debug(f'Put value {str(value)} to redis by key {key}')
        if ttl is not None:
            await self._redis.expire(key, ttl)

    async def delete(self, key: str):
        logger.debug(f'Deleted key {key} from redis')
        await self._redis.delete(key)
