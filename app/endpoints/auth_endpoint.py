from fastapi import APIRouter, status, HTTPException, Depends
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.exc import NoResultFound

from app.business.user import User
from app.models import UserModel
from app.schemas import TokenSchema, UserSchema, ProfileSchema
from app.core import data_base
from app.core.auth import (
    create_access_token,
    create_refresh_token,
    auth,
)

from app.utils import verify_password, object_as_dict

auth_route = APIRouter(tags=['Auth'])


@auth_route.post('/login', summary="Create access and refresh tokens for user", response_model=TokenSchema)
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    username = form_data.username
    password = form_data.password
    with data_base.session() as session:
        try:
            user = await User.get_by_email(username, session)
        except NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Incorrect email or password"
            )

        hashed_pass = user.password
        if not verify_password(password, hashed_pass):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Incorrect email or password"
            )

        return TokenSchema(
            access_token=create_access_token(user.id),
            refresh_token=create_refresh_token(user.id),
        )


@auth_route.get('/me', summary='Get details of currently logged in user', response_model=UserSchema)
async def get_me(user: UserModel = Depends(auth)):
    return UserSchema(
                id=user.id,
                email=user.email,
                role=user.role,
                phone_number=user.phone_number,
                is_active=user.is_active,
                profile=ProfileSchema(**object_as_dict(user.profile)),
            )
