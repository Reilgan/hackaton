from sqlalchemy import inspect

from app.utils.password.hash_pass import *
from app.utils.jwt_token import *
from app.utils.validations import *
from app.utils.to_camel_case import to_camel_case
from app.utils.classproperty import classproperty
from app.utils.verification_codes import digit4_verification_code
from app.utils.hash import dict_to_hash, str_to_hash


def object_as_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}


def clean_update_data(data: dict):
    update_data = {}
    for k, v in data.items():
        if v:
            update_data[k] = v
    return update_data
