from uuid import UUID
from datetime import date
from pydantic import BaseModel, Field, EmailStr, field_validator

from app.models import UserModel
from app.utils import validate_phone_number, validate_password
from app.schemas.exceptions import RegistrationFieldException


class ProfileSchema(BaseModel):
    first_name: str | None = None
    middle_name: str | None = None
    last_name: str | None = None
    birthday: date | None = None
    timezone: str | None = None


class CacheKeySchema(BaseModel):
    key: str = Field(..., description="cache hash key")


class CacheKeyCodeSchema(CacheKeySchema):
    verification_code: int = Field(..., description='verification email code')


class CreateUserSchema(BaseModel):
    email: EmailStr = Field(..., description="user email")
    phone_number: str = Field(..., description="user phone")
    password: str = Field(..., min_length=5, max_length=24, description="user password")
    role: UserModel.RoleEnum
    is_active: bool = True
    profile: ProfileSchema = None


    @field_validator('phone_number', mode='before')
    @classmethod
    def phone_validator(cls, value):
        if validate_phone_number(value) is False:
            raise RegistrationFieldException(field='phone_number')
        return value

    @field_validator('password', mode='before')
    @classmethod
    def password_validator(cls, value):
        if validate_password(value) is False:
            raise RegistrationFieldException(field='password')
        return value


class CacheCreateUserSchema(CreateUserSchema):
    verification_code: int = Field()


class UserAuthSchema(BaseModel):
    email: str = Field(..., description="user email")
    password: str = Field(..., min_length=5, max_length=24, description="user password")


class UpdateUserSchema(BaseModel):
    email: EmailStr = None
    phone_number: str = None
    password: str = None
    role: UserModel.RoleEnum = None
    is_active: bool = None
    profile: ProfileSchema = None

    @field_validator('phone_number', mode='before')
    @classmethod
    def phone_validator(cls, value):
        if validate_phone_number(value) is False:
            raise RegistrationFieldException(field='phone_number')
        return value

    @field_validator('password', mode='before')
    @classmethod
    def password_validator(cls, value):
        if validate_password(value) is False:
            raise RegistrationFieldException(field='password')
        return value


class UserSchema(BaseModel):
    id: UUID
    email: EmailStr
    phone_number: str
    is_active: bool
    role: UserModel.RoleEnum
    profile: ProfileSchema


class GetListUserSchema(BaseModel):
    users: list[UserSchema]
