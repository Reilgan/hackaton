import enum

import sqlalchemy as sa
from sqlalchemy.future import select
from sqlalchemy.orm import relationship, validates, Session
from app.core import OrmBaseModel
from app.models.mixin import UuidIdMixin, TimestampMixin
from app.core.database.db import data_base
from app.utils import get_hashed_password


class UserModel(UuidIdMixin, TimestampMixin, OrmBaseModel):
    __tablename__ = 'user'

    class RoleEnum(enum.Enum):
        operator = 'operator'
        admin = 'admin'

    email = sa.Column(sa.String, nullable=False, unique=True)
    password = sa.Column(sa.String, nullable=False)
    phone_number = sa.Column(sa.String, nullable=False, unique=True)
    is_active = sa.Column(sa.Boolean, default=True)

    # relations
    profile = relationship('ProfileModel', back_populates='user', uselist=False, cascade='all, delete-orphan',
                           passive_deletes=True)

    # password_history = relationship('PasswordHistoryModel', back_populates='user', uselist=True,
    #                                 cascade='all, delete-orphan', passive_deletes=True, lazy='noload')
    # login_history = relationship('LoginHistoryModel', back_populates='user', uselist=True,
    #                              cascade='all, delete-orphan', passive_deletes=True)
    role = sa.Column(sa.Enum(RoleEnum))

    @validates('password')
    def hashed_password(self, key, value: str):
        return get_hashed_password(value)
    #
    # def check_password(self, value: str) -> bool:
    #     def setter(value):
    #         self.password = value
    #
    #     return Hasher.check_hash_value(value, self.password, setter)

    @classmethod
    def is_email_exist(cls, email: str, session: Session = None) -> bool:
        query = select(UserModel).where(
            email == UserModel.email
        ).exists().select()

        if session:
            return session.execute(query).scalar()
        else:
            with data_base.session() as session:
                return session.execute(query).scalar()

