from fastapi import FastAPI
from sqlalchemy import create_engine
from starlette.staticfiles import StaticFiles

from app.settings import (DATABASE_URL, debug_mode, CACHE_URL)
from app.core.database.db import data_base
from app.endpoints import router
from app.middleware import middleware
from app.services import Cache

Cache.set_url(CACHE_URL)

app = FastAPI(
    debug=debug_mode(),
    middleware=middleware,
)
app.include_router(router)

engine = create_engine(DATABASE_URL, echo=False)


@app.on_event('startup')
async def on_startup():
    data_base.configure(engine)
    await Cache().connect()



