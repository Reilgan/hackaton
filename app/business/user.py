from typing import Any
from uuid import UUID

from sqlalchemy.orm import Session
from app.models import UserModel, ProfileModel

from app.schemas import CreateUserSchema
from app.core.database.db import data_base
from sqlalchemy import select, Row


class User:
    @classmethod
    async def create_in_db(cls, user_schema: CreateUserSchema, session: Session = None) -> UserModel:
        profile_model = ProfileModel(
            first_name=user_schema.profile.first_name,
            middle_name=user_schema.profile.middle_name,
            last_name=user_schema.profile.last_name,
            birthday=user_schema.profile.birthday,
            timezone=user_schema.profile.timezone
        )
        user_model = UserModel(
            email=user_schema.email,
            password=user_schema.password,
            phone_number=user_schema.phone_number,
            role=user_schema.role,
            is_active=True,
            profile=profile_model,
        )

        if session:
            session.add_all([profile_model, user_model])
            session.flush()
        else:
            with data_base.session() as session:
                session.add_all([profile_model, user_model])
                session.flush()

        return user_model

    @classmethod
    async def get_by_email(cls, email: str, session: Session = None) -> UserModel:
        if session:
            query = select(UserModel).filter_by(email=email)
            user = session.scalars(query).one()
            return user
        else:
            with data_base.session() as session:
                query = select(UserModel).filter_by(email=email)
                user = session.scalars(query).one()
                return user

    @classmethod
    async def get_by_id(cls, id: UUID, session: Session = None) -> UserModel:
        if session:
            query = select(UserModel).filter_by(id=id)
            user = session.scalars(query).one()
            return user
        else:
            with data_base.session() as session:
                query = select(UserModel).filter_by(id=id)
                user = session.scalars(query).one()
                return user

    @classmethod
    async def get_all(cls, session: Session = None) -> list[Row[tuple[UserModel]]] | Any:
        if session:
            users = session.query(UserModel).all()
            return list(users)
        else:
            with data_base.session() as session:
                users = session.query(UserModel).all()
                return list(users)



