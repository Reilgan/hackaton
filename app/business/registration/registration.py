from fastapi import status, HTTPException
from sqlalchemy.orm import Session
from app.models import UserModel

from app.business.registration.exceptions import EmailIsNotExist, InvalidVerificationCode
from app.business.user import User
from app.schemas import CreateUserSchema, CacheCreateUserSchema, CacheKeySchema, CacheKeyCodeSchema, \
    UserSchema, ProfileSchema
from app.services.cache import Cache
from app.utils import digit4_verification_code, dict_to_hash, object_as_dict
from app.settings import debug_mode
import json


class Registration:
    @classmethod
    async def cache_record_is_exist(cls, key: str) -> bytes | None:
        return await Cache().get(key)

    @classmethod
    async def send_email_code(cls, data: CreateUserSchema, session: Session = None) -> CacheKeySchema:
        if UserModel.is_email_exist(data.email, session):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="User with this email already exist"
            )
        value = data.model_dump()
        key = dict_to_hash(value)

        user_dict = await cls.cache_record_is_exist(key)
        if user_dict is not None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="email has already been sent, please wait to resend"
            )

        user_data = CacheCreateUserSchema(
            **value,
            verification_code=digit4_verification_code()
        )

        #Todo отправить письмо

        value = user_data.model_dump_json()
        await Cache().set(key=key, value=value, ttl=300)

        return CacheKeySchema(key=key)

    @classmethod
    async def enter_email_code(cls, data: CacheKeyCodeSchema, session: Session = None) -> UserSchema:
        user_bytes = await cls.cache_record_is_exist(data.key)
        if user_bytes is None:
            raise EmailIsNotExist(field='key')

        user_data = CacheCreateUserSchema(**json.loads(user_bytes.decode('utf-8')))

        if debug_mode():
            user = await User.create_in_db(CreateUserSchema(**user_data.model_dump()), session)
            return UserSchema(
                id=user.id,
                email=user.email,
                role=user.role,
                phone_number=user.phone_number,
                is_active=user.is_active,
                profile=ProfileSchema(**object_as_dict(user.profile))
            )
            # await Cache().delete(data.key)
            # return UserOutSchema(id=uuid4(), email='test@email.com')

        if user_data.verification_code == data.verification_code:
            user = await User.create_in_db(CreateUserSchema(**user_data.model_dump()), session)
            return UserSchema(
                id=user.id,
                email=user.email,
                role=user.role,
                phone_number=user.phone_number,
                is_active=user.is_active,
                profile=ProfileSchema(**object_as_dict(user.profile))
            )
            # await Cache().delete(data.key)
            # return UserOutSchema(id=uuid4(), email='test@email.com')
        else:
            raise InvalidVerificationCode(field='verification_code')

