from .check_db_connection_command import CheckDBConnectionCommand
from .create_default_admin import CreateDefaultAdminCommand

checkDBConnectionCommand = CheckDBConnectionCommand()
createDefaultAdmin = CreateDefaultAdminCommand()

